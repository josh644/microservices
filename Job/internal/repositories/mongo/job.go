package mongo

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"job-service/internal/core"
	"time"
)

type JobRepository struct {
	collection *mongo.Collection
}

func NewJobRepository(collection *mongo.Collection) *JobRepository {
	return &JobRepository{collection: collection}
}

func (repository *JobRepository) retrieveJobs(ctx context.Context, channel chan<- *[]core.Job) (err error) {
	var jobs []core.Job

	cursor, err := repository.collection.Find(ctx, bson.M{})

	if err != nil {
		return err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var job core.Job
		if err := cursor.Decode(&job); err != nil {
			return err
		}
		jobs = append(jobs, job)
	}

	if err := cursor.Err(); err != nil {
		return err
	}

	channel <- &jobs
	return nil
}

func (repository *JobRepository) GetAll(ctx context.Context) (*[]core.Job, error) {
	ctxTimeout, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	jobsChannel := make(chan *[]core.Job)
	var err error

	go func() {
		err = repository.retrieveJobs(ctxTimeout, jobsChannel)
	}()

	if err != nil {
		return nil, err
	}

	var jobs *[]core.Job

	select {
	case <-ctxTimeout.Done():
		fmt.Println("Processing timeout in mongo")
		break
	case jobs = <-jobsChannel:
		fmt.Println("Finished processing")
	}

	return jobs, nil
}
