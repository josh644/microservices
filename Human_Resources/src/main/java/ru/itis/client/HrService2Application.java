package ru.itis.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrService2Application {

    public static void main(String[] args) {
        SpringApplication.run(HrService2Application.class, args);
    }

}
